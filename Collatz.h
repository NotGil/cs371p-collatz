// ------------------------------
// projects/c++/collatz/Collatz.h
// Copyright (C) 2018
// Glenn P. Downing
// ------------------------------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <string>   // string
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * read two ints
 * @param s a string
 * @return a pair of ints, representing the beginning and end of a range, [i, j]
 */
pair<int, int> collatz_read (const string& s);

// ------------
// collatz_eval
// ------------

/**
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @return the max cycle length of the range [i, j]
 */
int collatz_eval (int i, int j);

// ------------
// simple_collatz_eval
// ------------

/**
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @return the max cycle length of the range [i, j]
 */
int simple_collatz_eval (int i, int j);


/**
 * @param i the starting number for Collatz sequence
 * @return the cycle length of i
 *
 */

int collatz_length(int i);

/**
 * @param a the beginning of the range, inclusive
 * @param j the end of the range, inclusive
 * @return the max cycle length of values already in the cache
 *
 */
int maxMid(int a, int b);

/**
 * @param i the starting number for Collatz sequence
 * @param acc the accumulator for the tail recursion
 * @return the cycle length of i+acc
 *
 */
int collatz_length_tr(long i, int acc);



// -------------
// collatz_print
// -------------

/**
 * print three ints
 * @param w an ostream
 * @param i the beginning of the range, inclusive
 * @param j the end       of the range, inclusive
 * @param v the max cycle length
 */
void collatz_print (ostream& w, int i, int j, int v);

// -------------
// collatz_solve
// -------------

/**
 * @param r an istream
 * @param w an ostream
 */
void collatz_solve (istream& r, ostream& w);

#endif // Collatz_h
